<?
require_once('Conexao.class.php');

class TipoServico extends Conexao{
	#Atributos da Classe Tipo Servi�o
	private $nome;
	private $chave;
	private $registro;

	#M�todo Construtor Tipo Servi�o
	function __construct(){
	}

	#M�todo Destrutor Tipo Servi�o
	function __desctruct(){
	}

	#M�todos Acessores Tipo Servi�o - SET
	function setChave($chave){
		$this->chave = $chave;
	}

	function setRegistro($registro){
		$registro = base64_decode($registro);
		$this->registro = $registro;
	}

	function setNome($nome){
		$this->nome = $nome;
	}

	#M�todos Acessores Tipo Servi�o - GET
	function getChave(){
		return $this->chave;
	}
	
	function getRegistro(){
		return $this->registro;
	}

	function getNome(){
		return $this->nome;
	}

	#Fun��o Cadastrar Tipo Servi�o
	function cadastrar($tipo){
  	$this->conectar();
	$sql = "INSERT INTO tipo_servico
				(
			 		tps_nu,
					tps_nome
					)
			VALUES
				(
				     ".$tipo->getChave().",
					 '".$tipo->getNome()."'
				)";
	$this->executaSql($sql);
	$this->fechaConexao();
	return "acao.php?msg=1";
}

#Fun��o Alterar Tipo Servi�o
function alterar($tipo){
  	$this->conectar();
	$sql = "UPDATE
					tipo_servico
			SET
			 		tps_nome = '".$tipo->getNome()."'
			WHERE
					tps_nu = ".$tipo->getRegistro()."";
	$this->executaSql($sql);
	$this->fechaConexao();
	return "acao.php?msg=2";
}

#Fun��o Excluir Tipo Servi�o
function excluir($tipo){
  	$this->conectar();
	$sql = "DELETE
			FROM
					tipo_servico
			WHERE
			     	tps_nu = ".$tipo->getRegistro()."";
	$this->executaSql($sql);
	$this->fechaConexao();
	return "acao.php?msg=3";
}

#Fun��o Listar Tipo Servi�o
function listarTipoServico(){
  	$this->conectar();
	$sql = "SELECT
					tps_nu,
					tps_nome
		   FROM
					tipo_servico
		   ORDER BY
		   			tps_nome";
	$rsTipo = $this->executaSql($sql);
	$this->fechaConexao();
	return $rsTipo;
}

#Fun��o Listar Tipo Servi�o 10 Registros
function listarTipoServico10(){
  	$this->conectar();
	$sql = "SELECT
					tps_nu,
					tps_nome
		    FROM
					tipo_servico
		    ORDER BY
		   			tps_nome
		    LIMIT 	10";
	$rsTipo = $this->executaSql($sql);
	$this->fechaConexao();
	return $rsTipo;
}

#Fun��o Visualizar Tipo Servi�o
function visualizar($tipo){
  	$this->conectar();
	$sql = "SELECT
					tps_nu,
					tps_nome
			FROM
					tipo_servico
			WHERE
			     	tps_nu = ".$tipo->getRegistro()."";
	$rsTipo = $this->executaSql($sql);
	$this->fechaConexao();
	return $rsTipo;
}

#Fun��o Pesquisar Tipo Servi�o
function pesquisar($tipo){
  	$this->conectar();
	$sql = "Select
						tps_nu,
						tps_nome
			FROM
						tipo_servico
			Where
						tps_nome Like '%".$tipo->getNome()."%'";
	$rsTipo = $this->executaSql($sql);
	$this->fechaConexao();
	return $rsTipo;
}

#Fun��o Verificar Novo Registro
function NextRegistro(){
  	$this->conectar();
	$sql = "SELECT nextval('tipo_servico_tps_nu_seq')";
	$rsTipo = $this->executaSql($sql);
	$this->fechaConexao();
	return $rsTipo;
}

} #Final da Classe

?>
