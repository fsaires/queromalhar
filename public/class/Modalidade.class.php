<?
require_once('Conexao.class.php');

class Modalidade extends Conexao{
	#Atributos da Classe Modalidade
	private $nome;
	private $chave;
	private $registro;

	#M�todo Construtor Modalidade
	function __construct(){
	}

	#M�todo Destrutor Modalidade
	function __desctruct(){
	}

	#M�todos Acessores Modalidade - SET
	function setChave($chave){
		$this->chave = $chave;
	}

	function setRegistro($registro){
		$registro = base64_decode($registro);
		$this->registro = $registro;
	}

	function setNome($nome){
		$this->nome = $nome;
	}

	#M�todos Acessores Modalidade - GET
	function getChave(){
		return $this->chave;
	}
	
	function getRegistro(){
		return $this->registro;
	}

	function getNome(){
		return $this->nome;
	}

	#Fun��o Cadastrar Modalidade
	function cadastrar($modalidade){
  	$this->conectar();
	$sql = "INSERT INTO modalidade
				(
			 		mod_nu,
					mod_nome
					)
			VALUES
				(
				     ".$modalidade->getChave().",
					 '".$modalidade->getNome()."'
				)";
	$this->executaSql($sql);
	$this->fechaConexao();
	return "acao.php?msg=1";
  }

#Fun��o Alterar Modalidade
function alterar($modalidade){
  	$this->conectar();
	$sql = "UPDATE
					modalidade
			SET
			 		mod_nome = '".$modalidade->getNome()."'
			WHERE
					mod_nu = ".$modalidade->getRegistro()."";
	$this->executaSql($sql);
	$this->fechaConexao();
	return "acao.php?msg=2";
}

#Fun��o Excluir Modalidade
function excluir($modalidade){
  	$this->conectar();
	$sql = "DELETE
			FROM
					modalidade
			WHERE
			     	mod_nu = ".$modalidade->getRegistro()."";
	$this->executaSql($sql);
	$this->fechaConexao();
	return "acao.php?msg=3";
}

#Fun��o Listar Modalidade
function listarModalidade(){
  	$this->conectar();
	$sql = "SELECT
					mod_nu,
					mod_nome
		   FROM
					modalidade
		   ORDER BY
		   			mod_nome";
	$rsModalidade = $this->executaSql($sql);
	$this->fechaConexao();
	return $rsModalidade;
}

#Fun��o Listar Modalidade 10 Registros
function listarModalidade10(){
  	$this->conectar();
	$sql = "SELECT
					mod_nu,
					mod_nome
		    FROM
					modalidade
		    ORDER BY
		   			mod_nome
		    LIMIT 	10";
	$rsModalidade = $this->executaSql($sql);
	$this->fechaConexao();
	return $rsModalidade;
}

#Fun��o Visualizar Modalidade
function visualizar($modalidade){
  	$this->conectar();
	$sql = "SELECT
					mod_nu,
					mod_nome
			FROM
					modalidade
			WHERE
			     	mod_nu = ".$modalidade->getRegistro()."";
	$rsModalidade = $this->executaSql($sql);
	$this->fechaConexao();
	return $rsModalidade;
}

#Fun��o Pesquisar Modalidade
function pesquisar($modalidade){
  	$this->conectar();
	$sql = "Select
						mod_nu,
						mod_nome
			FROM
						modalidade
			Where
						mod_nome Like '%".$modalidade->getNome()."%'";
	$rsModalidade = $this->executaSql($sql);
	$this->fechaConexao();
	return $rsModalidade;
}

#Fun��o Verificar Novo Registro
function NextRegistro(){
  	$this->conectar();
	$sql = "SELECT nextval('modalidade_mod_nu_seq')";
	$rsModalidade = $this->executaSql($sql);
	$this->fechaConexao();
	return $rsModalidade;
}
} #Final da Classe
?>
