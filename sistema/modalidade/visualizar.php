<? 
	include("../../public/inc/estrutura_admin.php"); 
	include("../../public/class/Modalidade.class.php");
	#Criar Objeto Modalidade
	$objModalidade = new Modalidade();
	$objModalidade->setRegistro($_GET[registro]);
	$query = $objModalidade->visualizar($objModalidade);
?>

<br>
<table align="center" border="0" width="500">
	<tr>
		<td align="center" class="titulo_adm">Modalidade</td>
	</tr>
</table>
<br>

<table align="center" border="1" bordercolor="FFB169" bordercolordark="white" cellspacing="0" cellpadding="2" width="500">
	<tr>
		<td bgcolor="FFA14A" colspan="2" align="center" class="subtitulo">Visualizar Modalidade</td>
	</tr>
	<? 	while($array = $objModalidade->mostraRegistros($query)) { ?>
	<tr>
        <td align="right" class="formulario" width="25%">Nome:</td>
		<td class="mtexto"> <? echo $array['mod_nome'] ?> </td>
	</tr>
	<? 	} ?>
	<tr>
		<td colspan="2" align="center"><input class="botao" type="submit" value="Voltar" onClick="javascript:window.location.href('index.php')"></td>	
	</tr> 
</table>
<br>

<? include("../../public/inc/rodape_admin.php"); ?>
